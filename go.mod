module chainmaker_web

go 1.16

require (
	chainmaker.org/chainmaker/common/v3 v3.0.1
	chainmaker.org/chainmaker/contract-utils v1.0.3
	chainmaker.org/chainmaker/pb-go/v3 v3.0.1
	chainmaker.org/chainmaker/sdk-go/v3 v3.0.1
	chainmaker.org/chainmaker/utils/v3 v3.0.1
	github.com/emirpasic/gods v1.12.0
	github.com/gin-gonic/gin v1.6.3
	github.com/gogo/protobuf v1.3.2
	github.com/hokaccha/go-prettyjson v0.0.0-20201222001619-a42f9ac2ec8e
	github.com/jinzhu/gorm v1.9.16
	github.com/spf13/viper v1.9.0
	go.uber.org/zap v1.17.0
)
